<?php
session_start();
if (isset($_SESSION['id'])) {
    header("Location: ../calendar.php");
    exit();
}
?>


<!DOCTYPE html>

<html lang=fr>


<body>

<head>
    <meta charset="UTF-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Mot de passe oublié - TropiCal-lr.fr</title>

    <!--FONT-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"/>

    <link rel="stylesheet" href="css/headerStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
    <!--FONTAWESOME-->
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" />
    <!--PERSONAL CSS-->
    <link href="css/indexStyle.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/forgotPasswordStyle.css">
</head>

<body>
<form action="dependencies/forgotPassword.php" method="post">
    <img class="mb-4" src="img/TropiCal.png" alt="TropiCal">
    <input type="email" id="email" class="form-control" placeholder="Adresse email" name="email" required autofocus>
    <button type="submit" class="btn btn-outline-secondary" id="pwd" name="submit">Générer un nouveau mot de passe</button>
    <?php
    $error = $_GET['error'];
    if (isset($error)) {
        switch ($error) {
            case 'empty':
                echo '<p class="error_red">Vous n\'avez pas rempli votre adresse mail !</p>';
                break;
            case 'email':
                echo '<p class="error_red">Merci de mettre un e-mail valide.</p>';
                break;
            case 'emailNotExist':
                echo '<p class="error_red">L\'email n\'existe pas</p>';
                break;
            case 'success':
                echo '<p class="success_green" <i class="fas fa-shield-check"></i>Votre mot de passe a été envoyé par mail</p>';
                break;
        }

    }
    ?>
    <div id="retour">
        <a href="index.php"><button type="button" class="btn btn-info">Retour</button></a>
    </div>
</form>
</body>

</html>
