<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: index.php");
    exit();
}
if (!($_SESSION['typeUser'] == "respModule" || $_SESSION['typeUser'] == "dirEtudes")) {
    header("Location: calendar.php");
    exit();
}
?>

<!DOCTYPE html>

<html lang=fr> 

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Inscription - TropiCal-lr.fr</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/headerStyle.css">
    <link rel="stylesheet" href="css/inscriptionStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>


<body>

<?php

include_once 'dependencies/header.php';
include_once 'dependencies/dbconnection.php';

?>

<section id="section-formulaire-connexion">
    <h2>Inscription</h2>
    <form action="dependencies/signup.php" method="post">

        <?php
        if ($_SESSION['typeUser'] == "dirEtudes") {
            echo '
            <select name="type" id="type">
                <option value="Responsable Module">Responsable Module</option>
                <option value="Titulaire">Titulaire</option>
                <option value="Vacataire">Vacataire</option>
            </select>
                ';
        } else {
            echo '
            <select name="type" id="type" style="display: none;">
                <option value="Vacataire">Vacataire</option>
            </select>
                ';
        }
        ?>

        <input type="text" placeholder="Nom" id="lastName" name="lastName">
        <input type="text" placeholder="Prénom" id="firstName" name="firstName">
        <input type="text" placeholder="Nom d'utilisateur" id="userName" name="userName">
        <input type="text" placeholder="E-mail" id="mailAdress" name="mailAdress">
        <?php
        if ($_SESSION['typeUser'] == "dirEtudes") {
            echo '<label for="idResp">Responsable :</label>
                   <select name="idResp" id="idResp">
                        <option value=""> --- Responsable de module --- </option>';
            $sql = "SELECT firstName, lastName, userName FROM Users WHERE typeUser = 'respModule'";
            $result = mysqli_query($connect, $sql);
            while ($row = mysqli_fetch_array($result)) {
                echo '<option value=' . $row['userName'] . '>' . $row['firstName'] . ' ' . $row['lastName'] . '</option>';
            }
            echo '</select><br /><br />';
        }
        ?>
        <?php
        $signup = $_GET['signup'];
        if (isset($signup)) {
            switch ($signup) {
                case 'empty':
                    echo '<h3 class="error_red">Vous n\'avez pas rempli tous les champs !</h3>';
                    break;
                case 'invalid':
                    echo '<h3 class="error_red">Merci de rentrer des Nom et Prénom constitués de lettres uniquement.</h3>';
                    break;
                case 'email':
                    echo '<h3 class="error_red">Merci de mettre un e-mail valide.</h3>';
                    break;
                case 'emailtaken':
                    echo '<h3 class="error_red">Votre adresse email est déjà prise.</h3>';
                    break;
                case 'usernametaken':
                    echo '<h3 class="error_red">Votre nom d\'utilisateur est déjà pris.</h3>';
                    break;
                case 'missingResp':
                    echo '<h3 class="error_red">Un vacataire doit avoir un responsable module.</h3>';
                    break;
                case 'noResp':
                    echo '<h3 class="error_red">Le nom d\'utilisateur du responsable de module n\'existe pas.</h3>';
                    break;
                case 'success':
                    echo '<h3 class="success_green">Votre inscription a été prise en compte.</h3>';
                    break;
            }
        }

        ?>

        <button class="btn btn-primary" type="submit" name="submit">S'inscrire</button>
        <a href="listUsers.php"><button type="button" class="btn btn-light">Retour</button></a>

    </form>
</section>
</body>
</html>
