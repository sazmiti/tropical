<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}
if (!($_SESSION['typeUser'] == "respModule" || $_SESSION['typeUser'] == "dirEtudes")) {
    header("Location: calendar.php");
    exit();
}

include_once 'dependencies/dbconnection.php';

?>

<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset="UTF-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>TropiCal | Mes disponibilités</title>
    <!--FONT-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"/>
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
    <!--PERSONAL CSS-->
    <link href="css/listeUsersStyle.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/headerStyle.css">
    <link rel="icon" type="image/png" href="img/icone.png">
    <script src="js/script.js"></script>

</head>

<body>

<?php

include_once 'dependencies/header.php';

?>

<section>
    <div>
        <?php
        if ($_SESSION['typeUser'] == "respModule"){
            echo '<h3 class="col-sm-4 col-md-4 col-lg-4 col-xl-4 offset-sm-1 offset-md-1 offset-lg-1 offset-xl-1" id="title-liste">Liste des vacataires</h3>';
        }
        if ($_SESSION['typeUser'] == "dirEtudes"){
            echo '<h3 class="col-sm-4 col-md-4 col-lg-4 col-xl-4 offset-sm-1 offset-md-1 offset-lg-1 offset-xl-1" id="title-liste">Liste des enseignants</h3>';
        }

        ?>
        <a href="inscription.php"><button class="btn btn-outline-success col-sm-2 col-md-2 col-lg-1 col-xl-1 offset-sm-3 offset-md-3 offset-lg-5 offset-xl-5" id="button-add-liste">Ajouter</button></a>
        <a href="calendar.php"><button class="btn btn-outline-info col-sm-2 col-md-2 col-lg-1 col-xl-1" id="button-add-liste">Retour</button></a>

    </div>
    <table class="table table-striped col-sm-12 col-md-12 col-lg-10 col-xl-10 offset-lg-1 offset-xl-1">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Nom d'utilisateur</th>
            <th>Adresse mail</th>
            <th>Type d'utilisateur</th>
            <th>Calendrier</th>
            <?php
            if($_SESSION['typeUser'] == "dirEtudes"){
                echo'<th>Exporter</th>';
            }
            ?>
            <th>Supprimer</th>
        </tr>
        </thead>
        <?php
        if ($_SESSION['typeUser'] == "respModule") {
            $idUsers = $_SESSION['id'];
            $sql = "SELECT * FROM Users WHERE idResp='$idUsers' ORDER BY lastName";
            $result = mysqli_query($connect, $sql);

            while ($row = mysqli_fetch_array($result)) {
                $id = $row['id'];
                echo '
                        <tr scope="row">
                            <td>' . $row['lastName'] . '</td>
                            <td>' . $row['firstName'] . '</td>
                            <td>' . $row['userName'] . '</td>
                            <td>' . $row['mailAddress'] . '</td>
                            <td>' . $row['typeUser'] . '</td>
                            <td><form method="post" action="dependencies/connectionUsers.php"><button type="submit" name="submit" class="btn btn-outline-success">Modifier</button><input name="id" value=" ' . $id . '"></form></td>
                            <td><form method="post" action="dependencies/suppressionUsers.php" onsubmit="return deleteUser()"><button type="submit" name="submit" class="btn btn-outline-danger">Supprimer</button><input name="id" value=" ' . $id . '"></form></td>
                        </tr>
                        ';
            }

        }
        if ($_SESSION['typeUser'] == "dirEtudes") {
            $idUsers = $_SESSION['id'];
            $sql = "SELECT * FROM Users WHERE id!='$idUsers' ORDER BY lastName";
            $result = mysqli_query($connect, $sql);

            while ($row = mysqli_fetch_array($result)) {
                $id = $row['id'];
                echo '
                        <tr>
                            <td>' . $row['lastName'] . '</td>
                            <td>' . $row['firstName'] . '</td>
                            <td>' . $row['userName'] . '</td>
                            <td>' . $row['mailAddress'] . '</td>
                            <td>' . $row['typeUser'] . '</td>
                            <td><form method="post" action="dependencies/connectionUsers.php"><button type="submit" name="submit" class="btn btn-outline-success">Modifier</button><input name="id" value=" ' . $id . '"></form></td>
                            <td><form method="post" action="dependencies/csvgenerator/mainCsv.php"><button type="submit" name="export" class="btn btn-outline-info">Télécharger</button><input name="id" value=" ' . $id . '"></form></td>
                            <td><form method="post" action="dependencies/suppressionUsers.php" onsubmit="return deleteUser()"><button type="submit" name="submit" class="btn btn-outline-danger">Supprimer</button><input name="id" value=" ' . $id . '"></form></td>
                        </tr>
                        ';
            }
        }
        ?>
    </table>
</section>
</body>
</html>
