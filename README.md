# TropiCal
Projet __PTS2__ de 6 étudiants de première année en __IUT Informatique__ à __La Rochelle__. :palm_tree: :calendar:

## Participants
- Brouillé Théo
- Caillaud Julien
- Delabarre Alexis
- Douteau Antoine
- Pilot Adrien
- Vannier Samuel

## Organisation
- [x] Présentation du projet par le client (M. Marchand)
- [x] Analyse et conception du projet
- [x] Développement du projet

## Liens utiles
- [GitLab](https://gitlab.univ-lr.fr/tbrouill/tropical)
- [Slack](https://pts2-a2.slack.com)
- [Trello](https://trello.com/b/XZFZoNML/pts2)
