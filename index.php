<?php
session_start();
if (isset($_SESSION['id'])) {
    header("Location: ../calendar.php");
    exit();
}
?>

<!DOCTYPE html>

<html lang=fr>

<head>
    <meta charset="UTF-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Connexion - TropiCal-lr.fr</title>

    <!--FONT-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"/>

    <link rel="stylesheet" href="css/headerStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
    <!--FONTAWESOME-->
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" />
    <!--PERSONAL CSS-->
    <link href="css/indexStyle.css" rel="stylesheet"/>
</head>

<body>
<form class="form-signin" action="dependencies/signin.php" method="post">
    <img class="mb-4" src="img/TropiCal.png" alt="TropiCal">
    <input type="text" id="userName" class="form-control" placeholder="Identifiant" name="userName" required autofocus>
    <input type="password" id="pwd" class="form-control" placeholder="Mot de passe" name="pwd" required>
    <?php
    $login = $_GET['login'];
    if (isset($login)) {
        switch ($login) {
            case 'error':
                echo '
                    <div class="alert alert-danger" role="alert">
                        <i class="fas fa-exclamation-triangle"></i>
                        Identifiant ou mot de passe invalide
                    </div>
                ';
                break;
        }
    }
    ?>
    <div class="row align-items-center">
        <div class="col-6">
            <a class="" href="mdpOublie.php">Mot de passe oublié ?</a>
        </div>
        <div class="col-6">
            <button id="btnconnexion" class="btn btn-outline-primary float-right" type="submit" name="submit">Se connecter</button>
        </div>

    </div>
</form>

</body>
</html>