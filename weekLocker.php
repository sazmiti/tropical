<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}

if (!($_SESSION['typeUser'] == "dirEtudes")) {
    header("Location: ../calendar.php");
    exit();
}


include 'dependencies/dbconnection.php';

?>

<!DOCTYPE html>
<html lang=fr>

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Verrouillage semaines - TropiCal-lr.fr</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/headerStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/inscriptionStyle.css  ">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<?php

include_once 'dependencies/header.php';

?>

<section id="section-formulaire-connexion">
    <h2>Verrouillage des semaines</h2>
    <form action="dependencies/locker.php" method="post">

        <select name="week" id="week">
            <?php

                $startActualWeek = date('Y-m-d',strtotime("last Monday"));
                $endActualWeek = date('Y-m-d',strtotime($startActualWeek . '+ 6 day'));

                $endSelection = date('Y-m-d',strtotime($startActualWeek . '+ 2 month'));; // samedi 27 juin 2020 + 1 jour

                $weekQuery = "SELECT `startRestr` FROM `Restriction` WHERE `startRestr` BETWEEN '$startActualWeek' AND '$endSelection' AND `titleRestr` = 'Generated' ORDER BY `startRestr`"; // tous les evenements du professeur
                $weekResult = $connect->query($weekQuery);

                $idWeekGenerated = 0;
                while ($weekRow = $weekResult->fetch_row()) {
                    $weekAlreadyGenerated[$idWeekGenerated] = strtotime($weekRow[0]);
                    $idWeekGenerated++;
                }

                $startWeek = strtotime($startActualWeek);
                $endWeek = strtotime($endActualWeek);
                $timestampEndSelection = strtotime($endSelection);

                for ($endWeek; $endWeek <= $timestampEndSelection; $endWeek = strtotime('+ 1 week',$endWeek)) {

                    if (in_array($startWeek,$weekAlreadyGenerated)) {
                        echo '<option disabled>'.'s' . date('W', $startWeek) . ' : ' . date('d/m/Y', $startWeek) . ' - ' . date('d/m/Y', $endWeek).'</option>';
                    } else {
                        echo '<option value="'.$startWeek.'">'.'s' . date('W', $startWeek) . ' : ' . date('d/m/Y', $startWeek) . ' - ' . date('d/m/Y', $endWeek).'</option>';
                    }
                    $startWeek = strtotime('+ 1 week',$startWeek);
                }

            ?>
        </select>

        <?php
        $result = $_GET['result'];
        if (isset($result)) {
            switch ($result) {
                case 'error':
                    echo '<h3 class="error_red">Une erreur est survenue.</h3>';
                    break;
                case 'success':
                    echo '<h3 class="success_green">Cette semaine a bien été verrouillée.</h3>';
                    break;
            }
        }

        ?>
        <div id="btns">
            <a href="parametres.php"><button type="button" class="btn btn-outline-info">Retour</button></a>
            <button type="submit" class="btn btn-outline-primary" name="submit">Verrouiller la semaine</button>
        </div>
    </form>
    <br>

</section>

</body>
</html>
