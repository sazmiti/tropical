<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang=fr>

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Paramètres - TropiCal-lr.fr</title>
    <link rel="stylesheet" href="css/parametresStyle.css  ">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/headerStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<?php

include_once 'dependencies/header.php';

?>

<div id="content">
    <div id="section-parametres-infos">
        <h2 id="titre">Vos informations </h2>
        <p><strong>Identifiant : </strong><?php echo $_SESSION['userName'] ?></p>
        <p><strong>Nom : </strong><?php echo $_SESSION['lastName'] ?></p>
        <p><strong>Prénom : </strong><?php echo $_SESSION['firstName'] ?></p>
        <p><strong>Adresse mail : </strong><?php echo $_SESSION['email'] ?></p>
    </div>
    <div id="section-parametres-boutons">
        <?php
        if ($_SESSION['typeUser'] == "dirEtudes") {
            echo '<a href="weekLocker.php"><button class="btn btn-outline-success" id="bverrouillage" type="button" name="bverrouillage">Verrouiller les semaines</button></a>';
        }
        ?>
        <a href="http://tropical-lr.fr/passwordChange.php">
            <button class="btn btn-outline-secondary" id="bmodifmdp" type="button" name="bmodifmdp">Modifier le mot de passe</button>
        </a>
        <?php
        if ($_SESSION['typeUser'] == "respModule" || $_SESSION['typeUser'] == "dirEtudes") {
            echo '
                <a href="inscription.php"><button class="btn btn-outline-secondary" id="bnewuser" type="button" name="bnewuser">Ajouter un nouvel utilisateur</button></a>';
        }
        if ($_SESSION['typeUser'] == "dirEtudes") {
            echo '<a href="vacances.php"><button class="btn btn-outline-secondary" id="bvacances" type="button" name="bvacances">Définir les semaines de vacances</button></a>';
        }
        ?>
        <form action="dependencies/logout.php" method="POST">
            <button class="btn btn-outline-danger" id="bdeconnection" type="submit" name="submit">Se déconnecter</button>
        </form>
    </div>
    <div id="retour">
        <a href="calendar.php"><button type="button" class="btn btn-outline-info">Retour</button></a>
    </div>
</div>
</body>
