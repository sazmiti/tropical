<?php

include_once ('dbconnection.php');

session_start();

if(isset($_SESSION['id'])){
    if(isset($_POST["id"]))
    {
        $idEvent = $_POST['id'];
        $title = $_POST['title'];
        $color = $_POST['color'];
        $type = "I";
        $start = $_POST['start'];
        $end = $_POST['end'];

        $idUser = $_SESSION['calendarId'];

        $start = substr($start,0, 24);
        $end = substr($end,0, 24);

        switch ($color){
            case "#2ecc71":
                $type="V";
                break;
            case "#70a1ff":
                $type="NV";
                break;
            case "#ff6348":
                $type="I";
                break;
        }

        $dstart = date_create_from_format("D F d Y H:i:s", $start);
        $dend = date_create_from_format("D F d Y H:i:s", $end);
        $start = $dstart->format('Y-m-d H:i');
        $end = $dend->format('Y-m-d H:i');


        $sql = "UPDATE Event SET comments='$title', disponibility='$type', start=TIMESTAMP('$start') , end=TIMESTAMP('$end'), idUser='$idUser' WHERE idEventUser='$idEvent' AND idUser='$idUser'";
        $result = mysqli_query($connect, $sql);
    }
} else {
    header("Location: /index.php");
    exit();
}



?>