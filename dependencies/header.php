<nav class="navbar navbar-expand-lg navbar-light" id="menunav">
    <a class="navbar-brand" href="../calendar.php"><img src="../img/TropiCal.png" alt="TropiCal.png" height="50px"
                                                        id="logo-menu"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <?php

        if (isset($_SESSION['id'])) {
            if($_SESSION['calendarId'] != $_SESSION['id']){
                echo '
                <ul class="navbar-nav mx-auto">
                    <span class="navbar-text" id="text-menu">Vous êtes en train de modifier le calendier de ' . $_SESSION['calendarFirst'] . ' ' . $_SESSION['calendarLast'] .'.</span>
                    <li class="nav-item"><form action="/dependencies/connectionUsers.php" method="post"><button class="btn btn-outline-light" type="submit" name="submit">Revenir à l\'utilisateur principal</button><input type="hidden" value=" ' . $_SESSION['id'] . '" name="id"></form></li>
                </ul>
                ';
            }

            if ($_SESSION['typeUser'] == "dirEtudes") {
                echo '
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="/dependencies/csvgenerator/mainZip.php" class="nav-link" id="link-dl"><button class="btn btn-outline-light">Télécharger les calendriers</button></a></li>
                    <li class="nav-item dropdown">
                        <button class="btn btn-outline-light dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img src="../img/user.png" alt="user.png" id="logo-user">' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'] . '
                        </button>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../parametres.php">Options</a>
                            <a class="dropdown-item" href="../listUsers.php">Enseignants</a>
                            <div class="dropdown-divider"></div>
                            <form action="/dependencies/logout.php" method="post" id="form-deconnection">
                                <button type="submit" name="submit" class="btn btn-outline-danger">Se déconnecter</button>
                            </form>
                        </div>
                    </li>
                </ul>
                ';
            }elseif($_SESSION['typeUser'] == "respModule"){
                echo '
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <button class="btn btn-outline-light dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img src="../img/user.png" alt="user.png" id="logo-user">' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'] . '
                        </button>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../parametres.php">Options</a>
                            <a class="dropdown-item" href="../listUsers.php">Vacataires</a>
                            <div class="dropdown-divider"></div>
                            <form action="/dependencies/logout.php" method="post" id="form-deconnection">
                                <button type="submit" name="submit" class="btn btn-outline-danger">Se déconnecter</button>
                            </form>
                        </div>
                    </li>
                </ul>
                ';
            }else{
                echo '
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <button class="btn btn-outline-light dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img src="../img/user.png" alt="user.png" id="logo-user">' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'] . '
                        </button>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../parametres.php">Options</a>
                            <div class="dropdown-divider"></div>
                            <form action="/dependencies/logout.php" method="post" id="form-deconnection">
                                <button type="submit" name="submit" class="btn btn-outline-danger">Se déconnecter</button>
                            </form>
                        </div>
                    </li>
                </ul>
                ';
            }
        }

        ?>



    </div>
</nav>