<?php

/*
 * Automatic generation of holidays.
 *
 * Run this script once a year.
 * It's possible to use a cron.
 */

include 'dbconnection.php';

    $year = date('Y',strtotime('+ 1 year'));

    $easterDate = easter_date($year);
    $easterDay = date('j', $easterDate);
    $easterMonth = date('n', $easterDate);
    $easterYear = date('Y', $easterDate);

        // Jours feries fixes

    $holidays = array(
    
        array(date('Y-m-d',mktime(0, 0, 0, 1, 1, $year)),'Jour de l\'an'),
        array(date('Y-m-d',mktime(0, 0, 0, 5, 1, $year)),'Fête du travail'),
        array(date('Y-m-d',mktime(0, 0, 0, 5, 8, $year)),'Victoire des alliés'),
        array(date('Y-m-d',mktime(0, 0, 0, 7, 14, $year)),'Fête Nationale'),
        array(date('Y-m-d',mktime(0, 0, 0, 8, 15, $year)),'Assomption'),
        array(date('Y-m-d',mktime(0, 0, 0, 11, 1, $year)),'Toussaint'),
        array(date('Y-m-d',mktime(0, 0, 0, 11, 11, $year)),'Armistice'),
        array(date('Y-m-d',mktime(0, 0, 0, 12, 25, $year)),'Noël'),

        // Jour feries qui dependent de paques
        array(date('Y-m-d',mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear)),'Lundi de Pâques'),
        array(date('Y-m-d',mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear)),'Ascension'),
        array(date('Y-m-d',mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear)),'Lundi de Pentecôte')
    );

    foreach ($holidays as $holiday) {

        $sql = "SELECT * FROM Restriction WHERE startRestr='$holiday[0]' AND endRestr='$holiday[0]'";
        $result = mysqli_query($connect, $sql);
        $resultcheck = mysqli_num_rows($result);
        if ($resultcheck < 1) {
            $holidaysQuery = "INSERT INTO Restriction(startRestr,endRestr,titleRestr) VALUES ('$holiday[0]','$holiday[0]',\"$holiday[1]\")";
            $holidaysResult = mysqli_query($connect,$holidaysQuery);
        }
    }

    header("Location: /index.php");
    exit();
?>