<?php


include_once ('dbconnection.php');

session_start();

$data = array();

if (isset($_SESSION['id'])){
    $idUser = $_SESSION['calendarId'];

    $sql = "SELECT * FROM Event WHERE idUser = '$idUser'";
    $result = mysqli_query($connect, $sql);

    while ($row = mysqli_fetch_array($result))
    {
        switch (strtolower($row['disponibility'])) {
            case "v":
                $color = "#2ecc71";
                break;
            case "nv":
                $color = "#70a1ff";
                break;
            case "i":
                $color = "#ff6348";
                break;
        }

        $dstart = date_create_from_format("Y-m-d H:i:s",$row['start']);
        $start = date_format($dstart,'Y-m-d\TH:i:s');

        $dend = date_create_from_format("Y-m-d H:i:s", $row['end']);
        $end = date_format($dend,'Y-m-d\TH:i:s');

        $data[] = array(
            'id'   => $row['idEventUser'],
            'title'   => $row['comments'],
            'start'   => $start,
            'end'   => $end,
            'color' => $color
        );

    }

    echo json_encode($data);

} else {
    header("Location: /index.php");
    exit();
}


?>