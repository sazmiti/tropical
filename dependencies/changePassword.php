<?php

include_once('dbconnection.php');

session_start();

$pwd = $_POST['pwd'];
$newpwd = $_POST['newPwd'];
$idUser = $_SESSION['id'];


if (empty($pwd) || empty($newpwd) || empty($idUser)) {

    header("Location: ../passwordChange.php?passChange=empty");
    exit();
} else {

    $sql = "SELECT * FROM Users WHERE id='$idUser'";
    $result = mysqli_query($connect, $sql);

    if ($row = mysqli_fetch_array($result)) {
        //DECODE THE PASSWORD
        $hashedpwdcheck = password_verify($pwd, $row['password']); // Check ancien mot de passe

        if ($hashedpwdcheck) {

            $hashedpwdcheck = password_verify($newpwd, $row['password']); // Check nouveau mot de passe different


            if(!$hashedpwdcheck) {

                $pattern = '/^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_@&#!?*^\]\[()~{}%:;,.-]+$/';

                if(preg_match($pattern,$newpwd)) {

                    $newHashpwd = password_hash($newpwd, PASSWORD_DEFAULT);
                    $sql = "UPDATE Users set password= '$newHashpwd' WHERE id= '$idUser'";
                    $result = mysqli_query($connect, $sql);
                    header("Location: ../passwordChange.php?passChange=success");
                    echo "reussi";
                    exit();

                } else {

                    header("Location: ../passwordChange.php?passChange=wrongCharacter");
                    exit();

                }

            } else {

                header("Location: ../passwordChange.php?passChange=samePassword");
                exit();

            }

        } else {
            header("Location: ../passwordChange.php?passChange=wrongPassword");
            exit();
        }

    }

}


?>
