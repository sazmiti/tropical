<?php
session_start();
if (!isset($_SESSION['id'])){
    header("Location: /index.php");
    exit();
}
if(!($_SESSION['typeUser'] == "respModule" || $_SESSION['typeUser'] == "dirEtudes")){
    header("Location: calendar.php");
    exit();
}

if (isset($_POST['submit'])) {

    /*Connection to the database*/
    include_once 'dbconnection.php';

    /*Getting the value of the form in the HTML*/
    $startWeek = htmlspecialchars($_POST['week']);

//ERROR CHECKER
    //CHECK FOR EMPTY
    if (empty($startWeek)) {
        header("Location: ../weekLocker.php?result=error");
        exit();
    }
    $dateStartWeek = date('Y-m-d',$startWeek);
    $dateEndWeek = date('Y-m-d',strtotime('+ 6 day',$startWeek));

    $sql = "INSERT INTO Restriction (startRestr, endRestr, titleRestr) 
            VALUES ('$dateStartWeek', '$dateEndWeek', 'Generated')";

    if( mysqli_query($connect, $sql) ) {

        //EXIT THE FILE
        header("Location: ../weekLocker.php?result=success");
        exit();

    } else {

        header("Location: ../weekLocker.php?result=error");
        exit();

    }

} else {
    header("Location: ../weekLocker.php");
    exit();
}
