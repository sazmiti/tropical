<?php

session_start();

if (isset($_POST['submit'])) {
    /*AJOUTER LA CONNECTION A LA BASE DE DONNEES*/
    include_once "dbconnection.php";

    $userName = htmlspecialchars($_POST['userName']);
    $pwd = htmlspecialchars($_POST['pwd']);

//ERROR CHECKER
    //CHECK FOR EMPTY

    if (empty($userName) || empty($pwd)) {
        header("Location: ../index.php?login=empty");
        exit();
    }
    else{
        //CHECK IF THE EMAIL EXIST
        $userName = strtolower($userName);
        $sql = "SELECT * FROM Users WHERE username = '$userName'";
        $result = mysqli_query($connect, $sql);
        $resultcheck = mysqli_num_rows($result);

        if ($resultcheck < 1) {
            header("Location: ../index.php?login=error");
            exit();
        }
        else{

            if ($row = mysqli_fetch_assoc($result)) {
                //DECODE THE PASSWORD
                $hashedpwdcheck = password_verify($pwd, $row['password']);

                if ($hashedpwdcheck == false) {
                    header("Location: ../index.php?login=error");
                    exit();
                }
                elseif ($hashedpwdcheck == true) {
                    //CONNECTION
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['firstName'] = $row['firstName'];
                    $_SESSION['lastName'] = $row['lastName'];
                    $_SESSION['userName'] = $row['userName'];
                    $_SESSION['typeUser'] = $row['typeUser'];
                    $_SESSION['email'] = $row['mailAddress'];
                    if($_SESSION['typeUser'] == "Vacataire"){
                        $_SESSION['resp'] = $row['idResp'];
                    }

                    /*GESTION DES DIFFERRENTS CALENDRIER*/
                    $_SESSION['calendarId'] = $_SESSION['id'];
                    $_SESSION['calendarLast'] = $_SESSION['lastName'];
                    $_SESSION['calendarFirst'] = $_SESSION['firstName'];

                    header("Location: ../calendar.php");
                    exit();
                }
            }

        }


    }
}
else{
    header("Location: ../index.php?login=error");
    exit();
}
?>