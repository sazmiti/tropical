<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../calendar.php");
    exit();
}

if (!isset($_POST['submit'])){
    header("Location: ../vacances.php");
    exit();
}else{
    $dateStart = $_POST['dateStart'];
    $dateEnd = $_POST['dateEnd'];
    $msg = "VACANCES";



    if (empty($dateStart) || empty($dateEnd)){
        header("Location: ../vacances.php?result=empty");
        exit();
    }else{
        include_once 'dbconnection.php';

        $dstart = substr($dateStart, 0,2);
        $mstart = substr($dateStart, 3, 2);
        $ystart = substr($dateStart, 6, 4);

        $dend = substr($dateEnd, 0,2);
        $mend = substr($dateEnd, 3, 2);
        $yend = substr($dateEnd, 6, 4);

        $sendStart = $ystart . '/' . $mstart . '/' . $dstart;
        $sendEnd = $yend . '/' . $mend . '/' . $dend;

        $sql = "INSERT INTO Restriction(startRestr, endRestr, titleRestr) VALUES('$sendStart', '$sendEnd', '$msg');";
        mysqli_query($connect, $sql);

        header("Location: ../vacances.php?result=success");
        exit();
    }

}
?>
