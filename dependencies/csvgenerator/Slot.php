<?php


class Slot{
    private $start;
    private $end;
    private $nbSlot;
    private $dispo;
    private $type;

    public function __construct($type, $start, $end, $nbSlot){
        $this->start = $start;
        $this->end = $end;
        $this->type = $type;
        $this->nbSlot = $nbSlot;
        $this->dispo = 'D';
    }

    public function setDispo($dispo)
    {
        $this->dispo = $dispo;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getDispo()
    {
        return $this->dispo;
    }

    public function getText(){
        switch (date('w', $this->start)){
            case 1 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "L+$this->nbSlot";
                    case 'D' :
                        return "L$this->nbSlot";
                    case 'NV' :
                        return "l$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
            case 2 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "Ma+$this->nbSlot";
                    case 'D' :
                        return "Ma$this->nbSlot";
                    case 'NV' :
                        return "ma$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
            case 3 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "Me+$this->nbSlot";
                    case 'D' :
                        return "Me$this->nbSlot";
                    case 'NV' :
                        return "me$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
            case 4 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "J+$this->nbSlot";
                    case 'D' :
                        return "J$this->nbSlot";
                    case 'NV' :
                        return "j$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
            case 5 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "V+$this->nbSlot";
                    case 'D' :
                        return "V$this->nbSlot";
                    case 'NV' :
                        return "v$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
            case 6 :
                switch (strtoupper($this->dispo)){
                    case 'V' :
                        return "S+$this->nbSlot";
                    case 'D' :
                        return "S$this->nbSlot";
                    case 'NV' :
                        return "s$this->nbSlot";
                    case 'I' :
                        return "   ";
                }
                break;
        }
        return "   ";
    }
}