<?php

class SchoolYear {
    private $start;
    private $end;
    private $listHoliday;
    private $listPHoliday;
    public function __construct($start, $end){
        $this->start = $start;
        $this->end = $end;
    }


    public function addHoliday($holiday){
        $this->listHoliday[] = $holiday;
    }
    public function addPHoliday($pHoliday){
        $this->listPHoliday[] = $pHoliday;
    }

    public function getWeek($nbWeek){
        $sWeek = strtotime('+ '. $nbWeek .' week', $this->start);
        $eWeek = strtotime('+ '. ($nbWeek+1) .' week', $this->start);
        $data[] = $sWeek;
        $data[] = $eWeek;
        return $data;
    }
    public function getStartYear(){
        return $this->start;
    }
    public function getEndYear(){
        return $this->end;
    }
}