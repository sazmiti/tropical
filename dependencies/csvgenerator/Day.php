<?php


class Day
{
    private $day;
    private $start;
    private $end;
    private $listEvent;
    private $listSlot;

    public function __construct($day){
        $this->day = $day;
        $this->start = $day;
        $this->end = strtotime( '+ 1 days' , $day);
        switch (date('w', $this->day)){
            /*Lundi, Mardi, Mercredi*/
            case 1:
            case 2:
            case 3:
                $this->listSlot = array(
                    new Slot('F',strtotime('00:00 -1 minutes', $this->day),strtotime('08:00', $this->day), 00),
                    new Slot('R',strtotime('08:00', $this->day),strtotime('10:00', $this->day), 11),
                    new Slot('R',strtotime('10:00', $this->day),strtotime('12:15', $this->day), 12),
                    new Slot('F',strtotime('12:15', $this->day),strtotime('14:00', $this->day), 00),
                    new Slot('R',strtotime('14:00', $this->day),strtotime('16:00', $this->day), 21),
                    new Slot('R',strtotime('16:00', $this->day),strtotime('18:15', $this->day), 22),
                    new Slot('F',strtotime('18:15', $this->day),strtotime('23:59 +2 minutes', $this->day), 00)
                );
                break;
            /*Jeudi, Samedi*/
            case 4:
            case 6:
                $this->listSlot = array(
                    new Slot('F',strtotime('00:00 -1 minutes', $this->day),strtotime('08:00', $this->day), 00),
                    new Slot('R',strtotime('08:00', $this->day),strtotime('10:00', $this->day), 11),
                    new Slot('R',strtotime('10:00', $this->day),strtotime('12:15', $this->day), 12),
                    new Slot('F',strtotime('12:15', $this->day),strtotime('23:59 +2 minutes', $this->day), 00)
                );
                break;
            /*Vendredi*/
            case 5:
                $this->listSlot = array(
                    new Slot('F',strtotime('00:00 -1 minutes', $this->day),strtotime('08:00', $this->day), 00),
                    new Slot('R',strtotime('08:00', $this->day),strtotime('10:00', $this->day), 11),
                    new Slot('R',strtotime('10:00', $this->day),strtotime('12:15', $this->day), 12),
                    new Slot('F',strtotime('12:15', $this->day),strtotime('14:00', $this->day), 00),
                    new Slot('R',strtotime('13:30', $this->day),strtotime('15:30', $this->day), 21),
                    new Slot('R',strtotime('15:30', $this->day),strtotime('17:45', $this->day), 22),
                    new Slot('F',strtotime('17:45', $this->day),strtotime('23:59 +2 minutes', $this->day), 00)
                );
                break;
        }

    }

    public function addEvent($start, $end, $disponibility){
        $this->listEvent[] = array($start, $end, $disponibility);
    }

    public function getStart()
    {
        return $this->start;
    }
    public function getEnd()
    {
        return $this->end;
    }

    public function getDay()
    {
        return $this->day;
    }

    public function associateEventToSlot(){


        /*Pour chaque évenement ce jour
            - Il faut vérifier si l'événement commence avant le début des créneaux
            - Il faut répartir les différentes contraintes dans les créneaux
            - Il faut vérifier si l'événement fini après les créneaux
        */
        foreach ($this->listEvent as $event){
            /*Recupere les informations de chaque évenement*/
            $eventStart = $event[0];
            $eventEnd = $event[1];
            $eventType = $event[2];
            $eventRegistered = false;
            $actualSlot = 0;

            while (!$eventRegistered){
                $startSlot = $this->listSlot[$actualSlot]->getStart();
                $endSlot = $this->listSlot[$actualSlot]->getEnd();

                if ($startSlot <= $eventStart && $eventStart < $endSlot){
                    if ($eventEnd <= $endSlot){
                        $this->listSlot[$actualSlot]->setDispo($eventType);
                        $eventRegistered = true;
                    }else{
                        $this->listSlot[$actualSlot]->setDispo($eventType);

                        $actualSlot++;
                        $startSlot = $this->listSlot[$actualSlot]->getStart();
                        $endSlot = $this->listSlot[$actualSlot]->getEnd();
                        while ($eventEnd > $endSlot){
                            $this->listSlot[$actualSlot]->setDispo($eventType);

                            $actualSlot++;
                            $startSlot = $this->listSlot[$actualSlot]->getStart();
                            $endSlot = $this->listSlot[$actualSlot]->getEnd();
                        }

                        if ($eventEnd > $startSlot){
                            $this->listSlot[$actualSlot]->setDispo($eventType);
                        }
                        $eventRegistered = true;
                    }
                }else{
                    $actualSlot++;
                }
            }

        }
    }

    public function getText(){
        $text = "";
        foreach ($this->listSlot as $slot){
            if($slot->getType() == 'R') {
                $text = $text . ' ' . $slot->getText();
            }
        }
        return $text;
    }


}