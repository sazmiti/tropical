<?php

/*
 * V1.1
 */

class RecuperatorEvent{
    private $listWeek;
    private $userId;
    private $endYear;

    public function __construct($listWeek, $userId, $endYear){
        $this->listWeek = $listWeek;
        $this->userId = $userId;
        $this->endYear = $endYear;

    }
    public function associateEventToWeek(){
        //include_once '../dbconnection.php'; // Connexion à la base de données

        $host_name = 'db5000104399.hosting-data.io';
        $database = 'dbs98903';
        $user_name = 'dbu131605';
        $password = 'AbsCorporation**1';
        $connect = mysqli_connect($host_name, $user_name, $password, $database);
        /*Récupération des valeurs de début et fin d'année pour la requête SQL*/


        $startYear = $this->listWeek[0]->getStart();
        $startYear = date('Y-m-d H:i:s', $startYear);
        $endYear = date('Y-m-d H:i:s', $this->endYear);
        $sql = "SELECT * FROM Event WHERE idUser ='$this->userId' AND `start` BETWEEN '$startYear' AND '$endYear' ORDER BY start";
        $result = mysqli_query($connect, $sql);


        $actualWeek = 0; // Permet de compté le nombre de mois

        /*Pour chaque ligne de résultat de la requête SQL*/
        while ($row = mysqli_fetch_array($result)){
            /*On récupere les variables la table*/
            $eventStart = strtotime($row['start']);
            $eventEnd = strtotime($row['end']);
            $eventType = $row['disponibility'];
            $eventRegistered = false;

            while(!$eventRegistered){
                /*Si le début de l'évenement est compris dans la semaine alors ...*/
                if ($this->listWeek[$actualWeek]->getStart() <= $eventStart && $eventStart < $this->listWeek[$actualWeek]->getEnd()){
                    /*Si la fin est compris dans la même semaines que le début allors*/
                    if ($eventEnd < $this->listWeek[$actualWeek]->getEnd()){
                            /*On envoie l'évenement à la semaine en question*/
                            $this->listWeek[$actualWeek]->addEvent($eventStart, $eventEnd, $eventType);
                            /*Confirme que l'événement est envoié*/
                            $eventRegistered = true;
                    /*Sinon (la fin de l'évenement n'est pas la même semaine que le début alors ...)*/
                    }else{
                        /*On ajoute l'évenement à la semaine actuelle de (debut à fin de semaine)*/
                        $this->listWeek[$actualWeek]->addEvent($eventStart, $this->listWeek[$actualWeek]->getEnd(), $eventType);
                        /*Puis on change de semaine*/
                        $actualWeek++;

                        /*Ensuite on vérifie si l'évenement se termine la semaine d'après (sinon alors on mais que la semaine est en évenement)*/
                        while (!$eventEnd < $this->listWeek[$actualWeek]->getEnd()){
                            $this->listWeek[$actualWeek]->addEvent($this->listWeek[$actualWeek]->getStart(), $this->listWeek[$actualWeek]->getEnd(), $eventType);
                            $actualWeek++;
                        }
                        /*Finalement, on l'évenement se termine et on peut validé la fin de l'évenement*/
                        $this->listWeek[$actualWeek]->addEvent($this->listWeek[$actualWeek]->getStart(), $eventEnd, $eventType);
                        $eventRegistered = true;

                    }
                /*Sinon (le début de l'évenement n'est pas cette semaine alors ...)*/
                }else{
                    /*On change de semaine*/
                    $actualWeek++;
                }
            }


        }

        foreach ($this->listWeek as $week){
            $week->associateEventToDay();
        }
        return true;
    }
}