<?php

/*
 * V2.0
 */

include_once '../dbconnection.php'; // Connexion à la base de données

require_once 'SchoolYear.php';
require_once 'ZipGenerator.php';
require_once 'CsvGenerator.php';
require_once 'Week.php';
require_once 'RecuperatorEvent.php';
require_once 'PublicHoliday.php';
require_once 'Holidays.php';
require_once 'Day.php';
require_once 'Slot.php';

/*
 * Generation du CSV individuel
 */

if(isset($_POST["export"])) {

    $schoolYear = new SchoolYear(strtotime('2019-09-02 00:00:00'),strtotime('2020-06-29 00:00:00')); // Fin de l'annee = dimanche matin 00:00:00

    $id = $_POST['id'];
    $sql = "SELECT userName FROM Users WHERE id='$id'";
    $result = mysqli_query($connect, $sql);
    $resultArray = mysqli_fetch_array($result);
    $pseudo = $resultArray['userName'];

    $csvGenerator = new CsvGenerator($id,$pseudo,$schoolYear);

    $csvGenerator->generateCsv();

    $filename = "EDT/$pseudo.csv";

    header("Location: $filename");
    exit();
}
header('Location: ../../listUsers.php');
exit();

?>
