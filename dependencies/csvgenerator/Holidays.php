<?php


class Holidays{
    private $start;
    private $end;

    public function __construct($start, $end){
        $this->start = $start;
        $this->end = $end;
    }

    public function getDuration(){
        $duration = strtotime($this->end) - strtotime($this->start);
        return $duration;
    }
    public function getWeek(){
        $sWeek = date_format($this->start,'W');
        $eWeek = date_format($this->end,'W');
        $data[] = $sWeek;
        $data[] = $eWeek;
        return $data;
    }
}