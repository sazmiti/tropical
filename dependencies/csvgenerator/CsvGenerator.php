<?php
/*
 * V1.5
 */

class CsvGenerator {

    private $idUser;
    private $pseudo;
    private $weeks;
    private $endYear;
    
    public function __construct($idUser,$pseudo,$schoolYear) {

        $this->idUser = $idUser;
        $this->pseudo = $pseudo;
        $idWeek = 0;

        $this->endYear = $schoolYear->getEndYear();

        while ($schoolYear->getWeek($idWeek)[1] <= $this->endYear) {
            $this->weeks[] = new Week($schoolYear->getWeek($idWeek)[0]);
            $idWeek++;
        }

    }

    public function generateCsv() {

        if(isset($this->weeks)) {

            $recuperatorEvent = new recuperatorEvent($this->weeks, $this->idUser, $this->endYear);

            if($recuperatorEvent->associateEventToWeek()) {

                $filename = "EDT/$this->pseudo.csv";

                if($file = fopen($filename, "w")) {

                    fputcsv($file,array('semaine (majuscule => construite) ',' debut - fin ',' creneaux (minuscules => contrainte souple)'),';');

                    foreach ($this->weeks as $week) {
                        $line = $line = array('s' . date('W', $week->getStart()) . ' ',' ' . date('d/m/Y', $week->getStart()) . ' - ' . date('d/m/Y', $week->getEnd()) . ' ');
                        $lineDay = $week->generateLine();
                        $line[2] = $lineDay;
                        fputcsv($file, $line,';');

                    }

                    if(fclose($file)) {

                        $replace = str_replace('"','',file_get_contents($filename));

                        if(file_put_contents($filename,$replace)) {

                            return true;

                        }
                    }
                }
            }
        }
        return false;
    }
}