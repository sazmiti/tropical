<?php

/*
 * V1.0
 */

class ZipGenerator {

    private $usersData;
    private $schoolYear;

    public function __construct($schoolYear) {
        $this->schoolYear = $schoolYear;
    }

    public function addUser ($userData) {

        if(isset($userData)) {

            $this->usersData[] = $userData;

            return true;
        }

        return false;
    }

    public function generateZip() {

        if(isset($this->usersData)) {

            $zip = new ZipArchive();

            if($zip->open('EDT/EDT.zip', ZipArchive::CREATE)) {

                foreach ($this->usersData as $data) {

                    $id = $data['id'];
                    $pseudo = $data['userName'];
                    $csvGenerator = new CsvGenerator($id,$pseudo,$this->schoolYear);

                    if ($csvGenerator->generateCsv()) {
                        $zip->addFile("EDT/$pseudo.csv");

                    } else {
                        return false;

                    }

                }

                if ($zip->close()) {

                    return true;

                }
            }

        }

        return false;

    }
}