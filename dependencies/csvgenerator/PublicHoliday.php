<?php


class PublicHoliday{
    private $day;

    public function __construct($day){
        $this->day = $day;
    }

    public function getWeekDay(){
        $dWeek = date_format($this->day,'W');
        $dDay = date_format($this->day,'w');
        $data[] = $dWeek;
        $data[] = $dDay;
        return $data;
    }
}