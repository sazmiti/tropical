<?php


class Week {

    private $start;
    private $end;
    private $events;
    private $listday;

    public function  __construct($start) {
        $this->start = $start;
        $this->end = strtotime('+ 5 day', $start);
        $day = $start;
        while ($day <= $this->end) {
            $this->listday[] = new Day($day);
            $day = strtotime('+ 1 day', $day);
        }

    }

    public function associateEventToDay() {

        $actualDay = 0; // Permet de compter le nombre de jour

        /*Pour chaque evenement*/
        foreach ($this->events as $event) {

            /*On récupere les details de l evenement*/
            $eventStart = $event[0];
            $eventEnd = $event[1];
            $eventType = $event[2];
            $eventRegistered = false;
            $actualDay = 0;

            while (!$eventRegistered){
                $startDay = $this->listday[$actualDay]->getStart();
                $endDay = $this->listday[$actualDay]->getEnd();

                if ($startDay <= $eventStart && $eventStart < $endDay){


                    if ($eventEnd <= $endDay){

                        $this->listday[$actualDay]->addEvent($eventStart, $eventEnd, $eventType);


                        $eventRegistered = true;
                    }else{
                        $this->listday[$actualDay]->addEvent($eventStart, $endDay, $eventType);


                        $actualDay++;
                        $startDay = $this->listday[$actualDay]->getStart();
                        $endDay = $this->listday[$actualDay]->getEnd();

                        while ($eventEnd > $endDay){
                            $this->listday[$actualDay]->addEvent($startDay, $endDay, $eventType);

                            $actualDay++;
                            $startDay = $this->listday[$actualDay]->getStart();
                            $endDay = $this->listday[$actualDay]->getEnd();
                        }

                        if ($eventEnd > $startDay){
                            $this->listday[$actualDay]->addEvent($startDay, $eventEnd, $eventType);
                        }
                        $eventRegistered = true;
                    }
                }else{
                    $actualDay++;
                }


            }

        }
        foreach ($this->listday as $day) {
            $day->associateEventToSlot();
        }
    }


    public function getStart() {
        return $this->start;
    }

    public function getEnd() {
        return $this->end;
    }

    public function addEvent($start,$end,$disponibility) {
        $this->events[] = array($start, $end, $disponibility);
    }

    public function generateLine() {
        $lineDay = '';
        foreach ($this->listday as $day) {
            $lineDay = $lineDay . '' . $day->getText();
        }
        return $lineDay;
    }

}