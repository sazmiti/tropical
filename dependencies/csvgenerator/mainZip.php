<?php


/*
 * V2.0
 */


include_once '../dbconnection.php'; // Connexion à la base de données

require_once 'SchoolYear.php';
require_once 'ZipGenerator.php';
require_once 'CsvGenerator.php';
require_once 'Week.php';
require_once 'RecuperatorEvent.php';
require_once 'PublicHoliday.php';
require_once 'Holidays.php';
require_once 'Day.php';
require_once 'Slot.php';

/*
 * Generation du ZIP
 */

$schoolYear = new SchoolYear(strtotime('2019-09-02 00:00'),strtotime('2020-06-28 00:00')); // Fin de l'annee = dimanche matin 00:00:00

$zipGenerator = new ZipGenerator($schoolYear);

$idPseudoQuery = "SELECT id, userName FROM Users ORDER BY id";
$idPseudoResult = mysqli_query($connect, $idPseudoQuery);
while ($idPseudoArray = mysqli_fetch_array($idPseudoResult)) {

    $zipGenerator->addUser($idPseudoArray);

}

$zipGenerator->generateZip();

header("Location: EDT/EDT.zip");
exit();

header('Location: /calendar.php');
exit();

?>


