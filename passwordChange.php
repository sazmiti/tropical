<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}

?>
<!DOCTYPE html>

<html lang=fr>

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>Changer votre mot de passe - TropiCal-lr.fr</title>
    <link rel="stylesheet" href="css/inscriptionStyle.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/headerStyle.css">
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<?php

include_once 'dependencies/header.php';

?>

<section id="section-formulaire-connexion">

    <h2>Modifier votre mot de passe</h2>
    <form action="dependencies/changePassword.php" method="post">

        <input type="password" placeholder="Mot de passe actuel" id="pwd" name="pwd">
        <input type="password" placeholder="Nouveau mot de passe" id="newPwd" name="newPwd">
        <?php
        $passResult = $_GET['passChange'];
        if (isset($passResult)) {
            switch ($passResult) {
                case 'empty':
                    echo '
                    <div class="alert alert-danger" role="alert">
                        Veuillez remplir tous les champs ci-dessus.
                    </div>
                ';
                    break;
                case 'wrongPassword':
                    echo '
                    <div class="alert alert-danger" role="alert">
                        Mot de passe actuel incorrect.
                    </div>
                ';
                    break;
                case 'samePassword':
                    echo '
                    <div class="alert alert-danger" role="alert">
                        Nouveau mot de passe identique.
                    </div>
                ';
                    break;
                case 'wrongCharacter':
                    echo '
                    <div class="alert alert-danger" role="alert">
                        Seuls les lettres, les chiffres, et les caractères spéciaux: _ - @ & # ! ? * ^ ] [ ( ) ~ { } % : ; , . sont autorisés.
                    </div>
                ';
                    break;
                case 'success':
                    echo '
                    <div class="alert alert-success" role="alert">
                        Votre mot de passe a bien été modifié.
                    </div>
                ';
                    break;
            }
        }
        ?>
        <div id="btns">
            <a href="parametres.php"><button class="btn btn-outline-info" type="button">Retour</button></a>
            <button class="btn btn-outline-primary" type="submit" name="submit">Confirmer</button>
        </div>
    </form>
</section>
</body>