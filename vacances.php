<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}

if (!($_SESSION['typeUser'] == "dirEtudes")){
    header("Location: ../parametres.php");
    exit();
}

?>

<!doctype html>
<html lang="fr">


<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Définition vacances - TropiCal-lr.fr</title>

    <link rel="icon" type="image/png" href="img/icone.png" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap" rel="stylesheet">
    <!--PERSONAL CSS-->
    <link href="css/vacancesStyle.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/headerStyle.css">
    <!--PERSONAL JS-->
    <script src="js/script.js"></script>
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!--MOMENT-->
    <script src="js/moment.js"></script>
    <script src="js/moment-fr.js"></script>
    <script>

        moment().format();
        options = {
            locale: moment.locale("fr"),
            format: 'YYYY/MM/DD'
        };
    </script>
    <!--BOOTSTRAP-->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/bootstrap.min.js"></script>
    <!--BOOTSTRAP DATETIMEPICKER-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
    <!--FONTAWESOME-->
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" />

</head>


<body>

<?php

include_once 'dependencies/header.php';

?>

<div id="section-selection-vacances">
    <h3>Ajout de vacances à l'emploi du temps</h3>
    <form action="dependencies/addHoliday.php" method="post">
        <div id="endDate">
            <h5>Début des vacances :</h5>
            <div class="input-group date" id="datePickerStart" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#datePickerStart" name="dateStart"/>
                <div class="input-group-append" data-target="#datePickerStart" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
        <div id="endDate">
            <h5>Fin des vacances :</h5>
            <div class="input-group date" id="datePickerEnd" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#datePickerEnd" name="dateEnd"/>
                <div class="input-group-append" data-target="#datePickerEnd" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
        <?php
        $result = $_GET['result'];
        switch ($result){
            case "empty" :
                echo '<p class="alert alert-danger text-result">Veuillez remplir tous les champs !</p>';
                break;
            case "success" :
                echo '<p class="alert alert-success text-result">Les vacances ont été ajoutée !</p>';
                break;
        }
        ?>
        <div class="text-center">
            <button class="btn btn-outline-primary center-b" type="submit" name="submit" id="button">Valider</button>
        </div>
    </form>
    <a href="parametres.php"><button type="button" class="btn btn-outline-info">Retour</button></a>
</div>

<script type="text/javascript">
    $(function () {
        $("#datePickerStart").datetimepicker("destroy");
        $('#datePickerStart').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY'
        });
        $('#datePickerEnd').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY'
        });
        $("#datePickerStart").on("change.datetimepicker", function (e) {
            $('#datePickerEnd').datetimepicker('minDate', e.date);
        });
        $("#datePickerEnd").on("change.datetimepicker", function (e) {
            $('#datePickerStart').datetimepicker('maxDate', e.date);
        });
    });
</script>
</body>


</html>



