<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: ../index.php");
    exit();
}

?>

<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset="UTF-8"/>
    <link rel="icon" type="image/png" href="img/icone.png" />
    <title>TropiCal | Mes disponibilités</title>


    <!--FONT-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"/>
    <!--PERSONAL CSS-->
    <link href="css/calendarStyle.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/headerStyle.css">
    <!--PERSONAL JS-->
    <script src="js/script.js"></script>
    <!--CALENDAR API CSS-->
    <link href="calendar/packages/core/main.css" rel="stylesheet"/>
    <link href="calendar/packages/daygrid/main.css" rel="stylesheet"/>
    <link href="calendar/packages/timegrid/main.css" rel="stylesheet"/>
    <!--CALENDAR API JS-->
    <script src="calendar/packages/core/main.js"></script>
	
    <script src="calendar/packages/daygrid/main.js"></script>
	<script src="calendar/packages/rrule/main.js"></script>
    <script src='calendar/packages/timegrid/main.js'></script>
    <script src="calendar/packages/interaction/main.js"></script>
    <script src='/calendar/packages/core/locales/fr.js'></script>
    <!--JQUERY-->
    <script src="js/jquery.min.js"></script>
    <!--MOMENT-->
    <script src="js/moment.js"></script>
    <script src="js/moment-fr.js"></script>
    <script>
        moment().format();
        options = {
            locale: moment.locale("fr"),
            format: 'DD/MM/YYYY HH:mm:ss'
        };
    </script>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
    <!--BOOTSTRAP DATETIMEPICKER-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
    <!--FONTAWESOME-->
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" />
    <!--CALENDAR IMPLEMENTATION-->
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let calendarEl = document.getElementById('calendar');
            let langue = 'fr';

            let calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ['interaction', 'dayGrid', 'timeGrid','rrule'],
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay'
                    },
                    minTime: "08:00:00",
                    height: "auto",
                    aspectRatio: 2.2,
                    maxTime: "19:00:00",
                    defaultView: 'timeGridWeek',
                    selectable: true,
                    locale: langue,
                    editable: true,
                    navLinks: true,
                    selectMirror: true,
                    eventLimit: true,
                    weekNumbers: true,
                    weekends:true,
                    hiddenDays: [ 0 ],
                    eventSources: [
                        {
                            url: '/dependencies/loadCalendar.php',
                        }],
                    events: [
                        {
                            daysOfWeek: [4],
                            startTime: '13:00',
                            endTime: '19:00',
                            rendering: 'background',
                            color: 'grey',
                        }, {
                            daysOfWeek: [6],
                            startTime: '13:00',
                            endTime: '19:00',
                            rendering: 'background',
                            color: 'grey',
                        }
                    ],
                    //Fonction pour empêcher event superposé
                    selectOverlap: function (event) {
                        if (event.title === $('#productDd option:selected').text()) {
                            return false;
                        }
                        return true;
                    },
                    eventOverlap: function(stillEvent, movingEvent) {
                        return stillEvent.title !== movingEvent.title;
                    },
                    //Fonction de création d'un event
                    select: function (arg) {
                        let id = Math.floor(Math.random() * 10000) + 1;
                        while (calendar.getEventById(id)) {
                            id = Math.floor(Math.random() * 10000) + 1;
                        }

                        calendar.addEvent({
                            id: id,
                            title: "",
                            start: arg.start,
                            end: arg.end,
                            allDay: arg.allDay,
                            color: "#ff6348"
                        });

                        showPopup("creation", calendar.getEventById(id));

                        calendar.unselect();
                    },
                    //Fonction de drag&drop event existant
                    eventDrop: function (info) {
                        $.ajax({
                            url: '/dependencies/updateCalendar.php',
                            data: {
                                id: info.event.id,
                                title: info.event.title,
                                start: info.event.start,
                                end: info.event.end,
                                color: info.event.backgroundColor
                            },
                            type: "POST"
                        });
                    },
                    //Fonction de clic sur event existant
                    eventClick: function (info) {
                        showPopup("modification", calendar.getEventById(info.event.id));
                    },
                    //Fonction de resize de l'event
                    eventResize: function (info) {
                        $.ajax({
                            url: '/dependencies/updateCalendar.php',
                            data: {
                                id: info.event.id,
                                title: info.event.title,
                                start: info.event.start,
                                end: info.event.end,
                                color: info.event.backgroundColor
                            },
                            type: "POST"
                        });
                    }
                }
            );
            calendar.render();
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                icons: {
                    time: 'far fa-clock',
                    date: 'fas fa-calendar-alt',
                    up: 'fas fa-arrow-up',
                    down: 'fas fa-arrow-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                    today: 'far fa-calendar-check-o',
                    clear: 'far fa-trash',
                    close: 'far fa-times'
                }
            });

            $("#datePickerStart").datetimepicker({
                disabledHours: [19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7],
                enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                stepping: 30
            });
            $("#datePickerEnd").datetimepicker({
                disabledHours: [19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7],
                enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                stepping: 30,
                useCurrent: false
            });

            $("#datePickerStart").on("change.datetimepicker", function (e) {
                $('#datePickerEnd').datetimepicker('minDate', e.date);
            });
            $("#datePickerEnd").on("change.datetimepicker", function (e) {
                $('#datePickerStart').datetimepicker('maxDate', e.date);
            });
        });
    </script>
</head>

<body>

<?php

include_once 'dependencies/header.php';

?>

<div id="calendar"></div>
<div class="modal fade" id="modalNewEvent">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Nouvel événement</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="changeEventProps('closeButton')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="type">
                    <div class="custom-radios">
                        <div>
                            <input type="radio" id="rVoulu" name="color" value="color-1">
                            <label for="rVoulu">
                                <span></span>
                                <h6 class="labelType">Voulu</h6>
                            </label>
                        </div>
                        <div>
                            <input type="radio" id="rNonVoulu" name="color" value="color-2">
                            <label for="rNonVoulu">
                                <span></span>
                                <h6 class="labelType">Non voulu</h6>
                            </label>
                        </div>
                        <div>
                            <input type="radio" id="rIndisponible" name="color" value="color-3" checked>
                            <label for="rIndisponible">
                                <span></span>
                                <h6 class="labelType">Indisponible</h6>
                            </label>
                        </div>
                    </div>
                    <div id="dates">
                        <div>
                            <h5>Début :</h5>
                            <div class="input-group date" id="datePickerStart" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#datePickerStart"/>
                                <div class="input-group-append" data-target="#datePickerStart" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        <div id="endDate">
                            <h5>Fin :</h5>
                            <div class="input-group date" id="datePickerEnd" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#datePickerEnd"/>
                                <div class="input-group-append" data-target="#datePickerEnd" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="reason">
                        <h5>Raison :</h5>
                        <textarea class="form-control" id="tfReason" placeholder="Facultatif" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="removeEvent()">Supprimer</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="changeEventProps('saveButton')">Enregistrer</button>
            </div>
        </div>
    </div>
</div>
</body>

</html>