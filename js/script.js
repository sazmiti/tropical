let actionType; // "creation" ou "modification"
let actualEvent;

function showPopup(action, event) {
    $("#modalNewEvent").modal({backdrop: "static", keyboard: false});
    $("#modalNewEvent").modal("toggle");

    actionType = action;
    actualEvent = event;

    switch (actualEvent.backgroundColor) {
        case "#2ecc71" :
            document.getElementById("rVoulu").checked = true;
            break;
        case "#70a1ff" :
            document.getElementById("rNonVoulu").checked = true;
            break;
        case "#ff6348" :
            document.getElementById("rIndisponible").checked = true;
            break;
    }
    $("#datePickerStart").datetimepicker("date", moment(actualEvent.start));
    $("#datePickerEnd").datetimepicker("date", moment(actualEvent.end));
    document.getElementById("tfReason").value = actualEvent.title;
}

function changeEventProps(buttonType) {
    if (buttonType === "saveButton") {
        let colorValue; // On prend le type d'événement et on l'accorde à l'événement
        if (document.getElementById("rVoulu").checked) {
            colorValue = "#2ecc71";
        } else if (document.getElementById("rNonVoulu").checked) {
            colorValue = "#70a1ff";
        } else {
            colorValue = "#ff6348";
        }
        let startDateValue = $("#datePickerStart").datetimepicker("date").toDate(); // On prend la date de d&ébut et on l'affecte à l'événement
        let endDateValue = $("#datePickerEnd").datetimepicker("date").toDate(); // On prend la date de fin et on l'affecte à l'événement
        let reasonValue = document.getElementById("tfReason").value; // On prend la veleur de la raison et on l'affecte au titre de l'événement

        // On accorde chaques valeurs à l'événement
        actualEvent.setProp("title", reasonValue);
        actualEvent.setStart(startDateValue);
        actualEvent.setEnd(endDateValue);
        actualEvent.setProp("color", colorValue);

        if (actionType === "creation") {
            $.ajax({
                url: "../dependencies/insertCalendar.php",
                data: {
                    id: actualEvent.id,
                    title: actualEvent.title,
                    start: actualEvent.start,
                    end: actualEvent.end,
                    color: actualEvent.backgroundColor
                },
                type: "POST"
            });
        } else if (actionType === "modification") {
            $.ajax({
                url: '../dependencies/updateCalendar.php',
                data: {
                    id: actualEvent.id,
                    title: actualEvent.title,
                    start: actualEvent.start,
                    end: actualEvent.end,
                    color: actualEvent.backgroundColor
                },
                type: "POST"
            });
        }

        $("#datePickerStart").datetimepicker("destroy");
    } else {
        if (actionType === "creation") {
            actualEvent.remove();
            $("#datePickerStart").datetimepicker("destroy");
        } else {
            $("#datePickerStart").datetimepicker("destroy");
        }
    }
}

function removeEvent() {
    // if (confirm("Voulez-vous vraiment supprimer cet événement ?")) {
        $.ajax({
            url: '../dependencies/deleteCalendar.php',
            type: 'POST',
            data: {
                id: actualEvent.id
            },
            success: function (json) {
                actualEvent.remove();
            }
        });
        $("#datePickerStart").datetimepicker("destroy");
    // } else {
    //     $("#datePickerStart").datetimepicker("destroy");
    //     //$("#modalNewEvent").modal("toggle");
    //     showPopup("modification", actualEvent);
    // }
}

function deleteUser() {
    if (confirm("Voulez-vous vraiment supprimer cet utilisateur ?")) {
        return true;
    } else {
        return false;
    }
}
